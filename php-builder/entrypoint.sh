#!/usr/bin/env bash

set -e

if [ "$SSH_PRIVATE_KEY" != "" ]; then
    eval $(ssh-agent -s)
    mkdir -p ~/.ssh
    echo "$SSH_PRIVATE_KEY" | ssh-add -
    [[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n" > ~/.ssh/config

    if [ "$SSH_USER" != "" ]; then
        [[ -f /.dockerenv ]] && echo -e "\tUser $SSH_USER\n" >> ~/.ssh/config
    fi

    [[ -f /.dockerenv ]] && echo -e "\n" >> ~/.ssh/config
fi

exec "$@"
